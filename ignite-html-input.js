import { IgniteTemplate } from "../ignite-html/ignite-template.js";

/**
 * Forces an input to only allow digits for input.
 * @param {Number} max Max number of digits allowed to be inputed. Default is -1 which disables this feature.
 * @returns {IgniteTemplate} This ignite template.
 */
 IgniteTemplate.prototype.inputDigits = function(max = -1) {
    this.on("keydown", e => {
        //If the input key isn't a digit, and it's not a backspace or escape or tab, ignore it.
        if ((e.key < '0' || e.key > '9') && e.key != 'Backspace' && e.key != 'Escape' && e.key != 'Tab' && e.key != 'Control' && e.ctrlKey == false) {
            e.preventDefault();
            return false;
        } else {
            //If we reached the max and the key isn't a special one then block this.
            if (max != -1 && e.target.value.length >= max && e.key != 'Backspace' && e.key != 'Escape' && e.key != 'Tab' && e.key != 'Control' && e.ctrlKey == false) {
                e.preventDefault();
                return false;
            }
            //If the control key is down, and it's not ctrl-a or ctrl-c or ctrl-v or ctrl-r then prevent
            else if (e.ctrlKey && e.key != 'a' && e.key != 'c' && e.key != 'v' && e.key != 'R' && e.key != 'r')
            {
                e.preventDefault();
                return false;
            }
        }
    });

    return this;
}

/**
 * Forces an input to only allow digits and periods for an input.
 * @param {Number} max Max number of digits allowed to be inputed. Default is -1 which disables this feature.
 * @returns {IgniteTemplate} This ignite template.
 */
IgniteTemplate.prototype.inputNumber = function(max = -1) {
    this.on("keydown", e => {
        //If the input key isn't a digit, and it's not a backspace or escape or tab, ignore it.
        if ((e.key < '0' || e.key > '9') && e.key != 'Backspace' && e.key != 'Escape' && e.key != 'Tab' && e.key != 'Control' && e.ctrlKey == false && e.key != '.') {
            e.preventDefault();
            return false;
        } else {
            //If we reached the max and the key isn't a special one then block this.
            if (max != -1 && e.target.value.length >= max && e.key != 'Backspace' && e.key != 'Escape' && e.key != 'Tab' && e.key != 'Control' && e.ctrlKey == false) {
                e.preventDefault();
                return false;
            }
            //If the control key is down, and it's not ctrl-a or ctrl-c or ctrl-v or ctrl-r then prevent
            else if (e.ctrlKey && e.key != 'a' && e.key != 'c' && e.key != 'v' && e.key != 'R' && e.key != 'r')
            {
                e.preventDefault();
                return false;
            }
        }
    });

    return this;
}

/**
 * Forces an input to only allow letters for input.
 * @param {Number} max Max number of letters allow to be inputed. Default is -1 which disables this feature. 
 * @param {Boolean} allowSpace Whether or not to allow spaces to be entered. Default is true.
 * @returns {IgniteTemplate} This ignite template.
 */
 IgniteTemplate.prototype.inputLetters = function(max = -1, allowSpace = true) {
    this.on("keydown", e => {
        //If the input key isn't a letter, and it's not a space, backspace or escape or tab, ignore it.
        if ((e.key < 'a' || e.key > 'z') && (e.key < 'A' || e.key > 'Z') && e.key != ' ' && e.key != 'Backspace' && e.key != 'Escape' && e.key != 'Tab' && e.key != 'Control' && e.ctrlKey == false) {
            e.preventDefault();
            return false;
        } else {
            //If we reached the max and the key isn't a special one then block this.
            if (max != -1 && e.target.value.length >= max && e.key != 'Backspace' && e.key != 'Escape' && e.key != 'Tab' && e.key != 'Control' && e.ctrlKey == false) {
                e.preventDefault();
                return false;
            }
            //If the control key is down, and it's not ctrl-a or ctrl-c or ctrl-v or ctrl-r then prevent
            else if (e.ctrlKey && e.key != 'a' && e.key != 'c' && e.key != 'v' && e.key != 'R' && e.key != 'r')
            {
                e.preventDefault();
                return false;
            }
            //If the key is space and space is not allowed prevent it.
            else if (allowSpace == false && e.key == ' ') {
                e.preventDefault();
                return false;
            }
        }
    });

    return this;
}

/**
 * Forces an input to only allow email based characters for input.
 * @param {Number} max Max number of letters allowed to be inputed. Default is -1 which disables this feature.
 * @returns {IgniteTemplate} This ignite template.
 */
 IgniteTemplate.prototype.inputEmail = function(max = -1) {
    this.on("keydown", e => {
        //If the input key isn't a letter, and it's not a space, backspace or escape or tab, ignore it.
        if ((e.key < 'a' || e.key > 'z') && (e.key < 'A' || e.key > 'Z') && (e.key < '0' || e.key > '9') && e.key != '@' && e.key != '.' && e.key != '-' && e.key != '_' && e.key != '+' && e.key != '~' && e.key != 'Backspace' && e.key != 'Escape' && e.key != 'Tab' && e.key != 'Control' && e.ctrlKey == false) {
            e.preventDefault();
            return false;
        } else {
            //If we reached the max and the key isn't a special one then block this.
            if (max != -1 && e.target.value.length >= max && e.key != 'Backspace' && e.key != 'Escape' && e.key != 'Tab' && e.key != 'Control' && e.ctrlKey == false) {
                e.preventDefault();
                return false;
            }
            //If the control key is down, and it's not ctrl-a or ctrl-c or ctrl-v or ctrl-r then prevent
            else if (e.ctrlKey && e.key != 'a' && e.key != 'c' && e.key != 'v' && e.key != 'R' && e.key != 'r')
            {
                e.preventDefault();
                return false;
            }
        }
    });

    return this;
}
